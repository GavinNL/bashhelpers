# Bash Helpers

This repo is a collection of bash aliases which I have separated into various
categories.

Each .sh file is independent. 

They may have some configuration variables, but default values are chosen if you do not
override them prior to sourcing the file


## Quick Start


```bash
cd .local
git clone https://gitlab.com/GavinNL/bashhelpers.git
echo "source $HOME/.local/bashhelpers/source_me_from_bashrc.sh" >> $HOME/.bashrc
```

If you have cloned the repo using git, the repo will update itself automatically
