#######################################################################################
# Configuration variables
#######################################################################################



#######################################################################################
# General Aliases
#######################################################################################
# export the current folder as a binary path
alias xp='export PATH=$PWD:$PATH'

# export the current folder as LD_LIBRARY_PATH
alias xlp='export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH'

# export the current folder as a python path
alias xpp='export PYTHONPATH=$PWD:$PYTHONPATH'

# current PID
alias pid=$$

alias cpr="rsync -ah --info=progress2"
# pid of parent
pid=$$

# pid of parent
ppid=$(grep PPid: /proc/${pid}/status | awk '{print $2'})

# pid of grandparent
pppid=$(grep PPid: /proc/${ppid}/status | awk '{print $2'})

# Get the PID tree of a process
# examples:  
#    1. get the pid tree of the current process all the way up to the root process
#     pidtree $$
#
alias gnl_pidtree='pstree -s -t -l -c -p -G -h '

# run a http server from the current working directory
alias gnl_www='python3 -m http.server'

# Pretty-print a git tree 
alias gnl_git_tree='git log --graph --pretty=oneline --abbrev-commit '

# Create a QR code from your clipboard
alias gnl_qrclip='xclip -o -s c | qrencode -o - | feh --force-aliasing -ZF -'

alias gnl_thesaurus='dict -d moby-thesaurus '

alias gnl_showpaths="echo "$PATH" | tr ':' '\n'"

# the PATH variable before this script messes with anything
export BASE_PATH=$PATH

#run a podman container with unix socket
alias xpodman='podman run -ti -e DISPLAY --rm --net=host -u 0 -v ~/.Xauthority:/root/.Xauthority:Z'

alias appy=apptainer



##################
function get_name_of_sink {
pacmd list-sinks | grep 'name:' | cut -c9-999 | rev | cut -c2-999 | rev | head -n $1 | tail -n 1
}

# Lists all audio sinks
function list_all_sinks {
pacmd list-sinks | grep 'name:' | cut -c9-999 | rev | cut -c2-999 | rev
}

# Get a dialog of available sinks to choose one.
function select_sink {
dialog --menu "Choose one:" 15 50 5 $(list_all_sinks | cat -n | tr '\n' ' ') --output-fd 1
}



function wav_to_mp3 {
ffmpeg -i $1  -vn -ac 2 -ab 192k -f mp3 $2
}



echoerr() { echo "$@" 1>&2; }

function gnl_record_audio {
M=$(select_sink)

filename=$HOME/Music/record_$(date +%s).wav

echoerr Recording audio to $filename. Press ctrl-C to stop.

parec -d $(get_name_of_sink $M).monitor --file-format=wav $filename

wav_to_mp3 $filename ${filename}.mp3

rm $filename

}

alias gnl_geolookup='curl -s http://ip-api.com/json/$(curl -s ifconfig.me) | jq'

function gnl_set_bpm {
    eyeD3 --bpm=$2 "$1"
}

function gnl_list_connections_on_port {
    lsof -nP -i TCP:$1
}
###################


