
#######################################################################################
# Use an android device as a webcam
#  You will need to install the following app on your android device
#        https://play.google.com/store/apps/details?id=com.pas.webcam
#
# Start the app and enable the capturing
#######################################################################################
gnl_enable_android_webcam()
{
    if [[ "$1" == "" ]]; then
        echo "Usage:  gnl_enable_android_webcam 192.168.1.126:8000"
        return 0
    fi
    # $1 = 192.168.1.126:8080
    sudo modprobe --remove v4l2loopback
    sudo modprobe v4l2loopback devices=1 max_buffers=2 exclusive_caps=1 card_label="VirtualCam #0"
    ffmpeg -i http://$1/videofeed -f v4l2 -pix_fmt yuv420p /dev/video0 
    sudo modprobe --remove v4l2loopback
}

