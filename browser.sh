#######################################################################################
FILE_LOCATION="${BASH_SOURCE[0]}"
while [ -h "$FILE_LOCATION" ]; do # resolve $FILE_LOCATION until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
  FILE_LOCATION="$(readlink "$FILE_LOCATION")"
  [[ $FILE_LOCATION != /* ]] && FILE_LOCATION="$DIR/$FILE_LOCATION" # if $FILE_LOCATION was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SOURCE_DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
#######################################################################################



#######################################################################################
# Configuration variables
#######################################################################################
HOME_TUNNEL_SSH=${HOME_TUNNEL_SSH:-"localhost"}

# Local port where the ssh tunnel will be created
HOME_TUNNEL_LOCAL_PORT=${HOME_TUNNEL_LOCAL_PORT:-9090}

CACHE_FOLDER=${CACHE_FOLDER:-${HOME}/.cache/HOME_ENV}
#######################################################################################

#######################################################################################
_firefox_profile() {
PROFILEDIR=$CACHE_FOLDER/firefox-$1-data
mkdir -p $PROFILEDIR
firefox -profile $PROFILEDIR -no-remote -new-instance
}

#######################################################################################

#######################################################################################
# Start the brave browser but use different data-directories for each
# for each service you are planning on using. This is so there is no
# cookies or other information shared between sites that are known
# to steal data
#######################################################################################
brave() {
    brave-browser --user-data-dir="$CACHE_FOLDER/brave-$1-data"
}

fbbrave() {
    brave fb
}

workbrave() {
    brave work
}

googlebrave() {
    brave google
}

tempbrave() {
    rm -rf $CACHE_FOLDER/brave-temp-data
    brave-browser --incognito --user-data-dir="$CACHE_FOLDER/brave-temp-data"
    rm -rf $CACHE_FOLDER/brave-temp-data
}

alias bankbrave='_firefox_profile bank'
alias gnl_tempbrave='tempbrave'
alias gnl_googlebrave='googlebrave'
alias gnl_workbrabe='workbrave'
alias gnl_brave='brave'
#######################################################################################




#######################################################################################
# Create a SSH socks5 socket and let your browser
# connect through that
#
# Usage: Open Two terminals
#     terminal 1: home_tunnel
#     terminal 2: home_browser
#######################################################################################
home_tunnel() {
    ssh -N -D ${HOME_TUNNEL_LOCAL_PORT} ${HOME_TUNNEL_SSH}
}

home_browser() {
    brave-browser --user-data-dir="$CACHE_FOLDER/brave-tunnel" --proxy-server="socks5://localhost:${HOME_TUNNEL_LOCAL_PORT}"
}

alias gnl_home_tunnel='home_tunnel'
alias gnl_home_browser='home_browser'
#######################################################################################

