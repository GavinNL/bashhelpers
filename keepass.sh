#######################################################################################
# 1. Mount a remote sshfs folder 
# 2. 
#######################################################################################
CACHE_FOLDER=${CACHE_FOLDER:-${HOME}/.cache/HOME_ENV}
SSH_MOUNT_FOLDER=${SSH_MOUNT_FOLDER:-$HOME/.local/mnt}
CACHED_PASSWORD_FILE_DEFAULT=${CACHE_FOLDER}/cached_password/${HOSTNAME}/${HOSTNAME}.kdbx
CACHED_PASSWORD_FILE=${CACHED_PASSWORD_FILE:-${CACEHD_PASSWORD_FILE_DEFAULT}}

remote_password2()
{
    #######################################################################################
    # This should be a sshf mounted folder. It is the MASTER PASSWORD LOCATION
    MOUNTED_REMOTE_PASSWORD_MASTER_FILE=$1
    #######################################################################################

    
    MASTER_FOLDER=$(dirname ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE})
    MASTER_FILE=$(basename ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE})

    CACHED_PASSWORD_FOLDER=$(dirname ${CACHED_PASSWORD_FILE})    
    if [ ! -f ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE} ]; then
        echo "Remotely mounted file ($MOUNTED_REMOTE_PASSWORD_MASTER_FILE) does not exist. Make sure this is an SSHFS mount"
        echo "Opening cached file in read only mode instead"

        if [ -f ${CACHED_PASSWORD_FILE} ]; then
            chmod 400 ${CACHED_PASSWORD_FILE} 
            keepassx ${CACHED_PASSWORD_FILE}
        else 
            echo "Cannot find a cached password file."
            return
        fi
        return 
    fi


    if [ -f ${CACHED_PASSWORD_FILE} ]; then
        chmod 744 ${CACHED_PASSWORD_FILE}
    fi 

    # Copy the remote mounted file to a cached location
    echo " - Copying remote mounted file to cache location: ${CACHED_PASSWORD_FOLDER}"
    mkdir -p ${CACHED_PASSWORD_FOLDER}
    cp ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE} ${CACHED_PASSWORD_FILE}

    # Remote the keepass config
    rm -rf $HOME/.config/keepassx

    # call keepass on the cached file
    keepassx ${CACHED_PASSWORD_FILE}

    # remove the config file if it was created
    rm -rf $HOME/.config/keepassx


    # compute the new ashes
    CACHED_SUM=$(sha256sum ${CACHED_PASSWORD_FILE} | cut -c1-64)
    MASTER_SUM=$(sha256sum ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE} | cut -c1-64)

    echo " - Checking Checksums:"
    echo "      master: ${CACHED_PASSWORD_FILE}"
    echo "          sum: ${CACHED_SUM}"
    echo "      master: ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE}"
    echo "          sum: ${MASTER_SUM}"

    # if they are not the same
    if [ ! "${CACHED_SUM}" == "${MASTER_SUM}" ]; then

        echo " - CHECKSUMS are different:"
        NEW_NAME=${MASTER_FOLDER}/${MASTER_FILE}.$(date "+%Y%m%d%H%M%S").${HOSTNAME}.bak   

        echo " - Backing up Master db to ${NEW_NAME}"

        # Back up the old master password database
        cp ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE}  ${NEW_NAME}

        if [ -f ${NEW_NAME} ]; then
            echo " - Backed up successfully" 

            # and copy the new one over
            cp ${CACHED_PASSWORD_FILE} ${MOUNTED_REMOTE_PASSWORD_MASTER_FILE}
        fi
    fi
}



try_unmount_sshfs()
{
    RET=1
    while [ "${RET}" != "0" ]
    do
        sleep 1
        fusermount -u $1
        RET=$?
    done
}

gnl_remote_password()
{
    SSH_URI=${KEEPASS_SSH_URI}
    MOUNT_LOC=${SSH_MOUNT_FOLDER}/.keepass_ssh
    SSH_PORT=${KEEPASS_SSH_PORT}
    MASTER_FILE_NAME=${KEEPASS_MASTER_FILE_NAME}

    if [ "${SSH_URI}" == "" ]; then
        echo "Error: KEEPASS_SSH_URI has not been set. Cannot open a remote password database"
        return 1
    fi
    if [ "${SSH_PORT}" == "" ]; then
        echo "Error: KEEPASS_SSH_PORT has not been set. Cannot open a remote password database"
        return 1
    fi
    if [ "${KEEPASS_MASTER_FILE_NAME}" == "" ]; then
        echo "Error: KEEPASS_MASTER_FILE_NAME has not been set. Cannot open a remote password database"
        return 1
    fi
    mkdir -p $MOUNT_LOC
    echo "Mounting SSHFS at ${MOUNT_LOC}"
    echo  sshfs ${SSH_URI} $MOUNT_LOC -o Ciphers=chacha20-poly1305@openssh.com -p $SSH_PORT
    sshfs ${SSH_URI} $MOUNT_LOC -o Ciphers=chacha20-poly1305@openssh.com -p $SSH_PORT

    if [ ! -f $MOUNT_LOC/$MASTER_FILE_NAME ]; then
        echo "Error: $MOUNT_LOC/$MASTER_FILE_NAME does not exist. Cannot open remote password database"
        try_unmount_sshfs ${MOUNT_LOC}        
        return 1
    fi
    remote_password2 $MOUNT_LOC/$MASTER_FILE_NAME

    echo "Unmounting SSHFS at ${MOUNT_LOC}"
    try_unmount_sshfs ${MOUNT_LOC}
}

gnl_passwords_ro()
{
   # CACHED_PASSWORD_FOLDER=${CACHE_FOLDER}/cached_password/$HOSTNAME
    #CACHED_PASSWORD_FILE=${CACHED_PASSWORD_FOLDER}/passwords.kdbx
    chmod 400 ${CACHED_PASSWORD_FILE} 
    keepassx ${CACHED_PASSWORD_FILE}
}



