#######################################################################################
# 1. Mount a remote sshfs folder 
# 2. 
#######################################################################################
CACHE_FOLDER=${CACHE_FOLDER:-${HOME}/.cache/HOME_ENV}

WEB_DAV=https://ewebdav.pcloud.com
PASSWORD_PATH=/Passwords/passwords.kdbx
PASSWORD_CACHE=${PASSWORD_CACHE:-${HOME}/Documents/passwords.kdbx}
WEBDAV_PASSWORDS_FOLDER_PATH=/Passwords


gnl_passwords_ro2()
{
    chmod 400 ${PASSWORD_CACHE} 
    keepassx ${PASSWORD_CACHE}
    chmod 644 ${PASSWORD_CACHE} 
}

# generate unique name
function generate_new_kdb_filename()
{
    date +%Y_%m_%d_%H_%M_%S
}

function checksum()
{
    md5sum $1 | awk '{print $1}'
}

function _downloadLatestDB()
{
    LAST_PASSWORD_PATH=$(list $WEBDAV_PASSWORDS_FOLDER_PATH | tail -n 1)
    download $LAST_PASSWORD_PATH $PASSWORD_CACHE
}


gnl_passwords()
{
    if [[ "$(which rclone)" == "" ]]; then
        echo "Rclone not installed"
        return 1
    fi

    if [[ "$(rclone listremotes | grep Pcloud | wc -l)" != "1" ]]; then
        echo rclone remote, Pcloud does not exist
        return 1
    fi

    if [[ ! -f "${PASSWORD_CACHE}" ]]; then
        echo ${PASSWORD_CACHE} does not exist. Please download an initial one from your pcloud
        return 1
    fi

    initialChecksum=$(checksum $PASSWORD_CACHE)
    echo Initial Checksum: $initialChecksum

    keepassx $PASSWORD_CACHE

    finalChecksum=$(checksum $PASSWORD_CACHE)
    echo Initial Checksum: $finalChecksum

    if [[ "${initialChecksum}" != "${finalChecksum}" ]]; then

        newFileName=$(date +%Y_%m_%d_%H_%M_%S).kdbx
        #newFileName=$(generate_new_kdb_filename).kdbx

        tmpFolder=$(mktemp -d)

        cp ${PASSWORD_CACHE} ${tmpFolder}/${newFileName}

        echo "File has changed, we need to upload: /tmp/${newFileName}" to "Pcloud:${WEBDAV_PASSWORDS_FOLDER_PATH}"

        echo " Uploading ${tmpFolder}/${newFileName} --> Pcloud:${WEBDAV_PASSWORDS_FOLDER_PATH}"
        rclone copy ${tmpFolder}/${newFileName} Pcloud:${WEBDAV_PASSWORDS_FOLDER_PATH}

        
        mkdir ${tmpFolder}/redownload
        echo " Downloading Pcloud:${WEBDAV_PASSWORDS_FOLDER_PATH}/${newFileName} --> ${tmpFolder}/redownload "
        rclone copy  Pcloud:${WEBDAV_PASSWORDS_FOLDER_PATH}/${newFileName} ${tmpFolder}/redownload

        postDownloadChecksum=$(checksum ${tmpFolder}/redownload/${newFileName})
        
        echo " Downloaded checksum ${postDownloadChecksum}"
        
        if [[ "${finalChecksum}" != "${postDownloadChecksum}" ]]; then  
            echo "Checksum of modified database is not the same as the checksum of the uploaded database. Something went wrong!"
        else 
            echo "Database uploaded and checked successfully"
            rm -rf ${tmpFolder}
        fi

    fi

}


