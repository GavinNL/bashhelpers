#######################################################################################
FILE_LOCATION="${BASH_SOURCE[0]}"
while [ -h "$FILE_LOCATION" ]; do # resolve $FILE_LOCATION until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
  FILE_LOCATION="$(readlink "$FILE_LOCATION")"
  [[ $FILE_LOCATION != /* ]] && FILE_LOCATION="$DIR/$FILE_LOCATION" # if $FILE_LOCATION was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SOURCE_DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
#######################################################################################


#######################################################################################
# Configuration variables
#######################################################################################
# The location where all SSHFS mounts will be mounted
# All ssh mounts will be in SSH_MOUNT_FOLDER/MOUNT_NAME
SSH_MOUNT_FOLDER=${SSH_MOUNT_FOLDER:-$HOME/.local/mnt}
#######################################################################################



# A special internal function to mount a SSHFS
#
# Usage:
#
#   mount_sshfs_base  USERNAME@ADDRESS:/path/to/mount 22 MY_NAME
#
# This will create the actual mount in $SSH_MOUNT_FOLDER/MY_NAME
# and also create a symlink in your $HOME folder to the mount point
#
# You probably shouldn't call this function directrly from the command
# line. Instead create your own functions  
mount_sshfs_base() {

    SSH_LINE=$1
    PORT=$2
    MOUNT_NAME=$3

    if [ ${SSH_LINE} == "" ]; then
        exit 1
    fi
    if [ ${PORT} == "" ]; then
        exit 1
    fi
    if [ ${MOUNT_NAME} == "" ]; then
        exit 1
    fi
    
    mkdir -p $SSH_MOUNT_FOLDER/$MOUNT_NAME
    sshfs ${SSH_LINE} $SSH_MOUNT_FOLDER/$MOUNT_NAME -o Ciphers=chacha20-poly1305@openssh.com -p $PORT

    if [ ! -L $HOME/$MOUNT_NAME ]; then
      ln -s $SSH_MOUNT_FOLDER/$MOUNT_NAME $HOME/$MOUNT_NAME 
    fi

}


#######################################################################################

