#######################################################################################
FILE_LOCATION="${BASH_SOURCE[0]}"
while [ -h "$FILE_LOCATION" ]; do # resolve $FILE_LOCATION until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
  FILE_LOCATION="$(readlink "$FILE_LOCATION")"
  [[ $FILE_LOCATION != /* ]] && FILE_LOCATION="$DIR/$FILE_LOCATION" # if $FILE_LOCATION was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SOURCE_DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
#######################################################################################


#######################################################################################
# Enable a NIX package manager enviorment. You will ned to have the 
# NIX package manager installed. The NIX package manager installation
# updtes your profile.rc file to include sourcing a script. 
# I chose to separate it and only use NIX when I want to.
#
# Either execute NIX from the command line to enable to env
# or run "NIX exe" to run a nix package
#######################################################################################
NIX()
{
    FILE=$HOME/.nix-profile/etc/profile.d/nix.sh

    if ! test -f "$FILE"; then
        echo "NIX package manager may not be installed. Could not find $FILE"
        exit 1
    fi

    function nix-install() {
      nix-env -iA "$@"
    }

    function nix-remove() {
      nix-env --uninstall "$@"
    }

    function nix-update() {
      nix-channel --update "$@"
    }

    function nix-upgrade() {
      nix-env -u "$@"
    }

    function nix-depends() {
      nix-store --query --references $(nix-instantiate '<nixpkgs>' -A $1)
    }

    nix-info() {
        nix-env -qa --description "$@"
    }

    nix-test() {
        echo hello "$@"
    }

    nix-search() {
        nix-env -qaP "$@"
    }


    export -f nix-search nix-info nix-depends nix-upgrade nix-remove nix-update nix-install nix-test
    if [[ "$1" == "" ]]; then
        CURR_ENV="${CURR_ENV}|NIX" bash --init-file <(cat /etc/profile ~/.bashrc "${FILE}") 
    else
        CURR_ENV="${CURR_ENV}|NIX" bash --init-file <(cat /etc/profile ~/.bashrc "${FILE}") -ic "$@"
    fi
    unset -f nix-search nix-info nix-depends nix-upgrade nix-remove nix-update nix-install nix-test
}


