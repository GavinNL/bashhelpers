#######################################################################################
FILE_LOCATION="${BASH_SOURCE[0]}"
while [ -h "$FILE_LOCATION" ]; do # resolve $FILE_LOCATION until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
  FILE_LOCATION="$(readlink "$FILE_LOCATION")"
  [[ $FILE_LOCATION != /* ]] && FILE_LOCATION="$DIR/$FILE_LOCATION" # if $FILE_LOCATION was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SOURCE_DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
#######################################################################################


#######################################################################################
# Configuration variables
#######################################################################################
# The location where all notes files will be stored
#export NOTES_FOLDER=${NOTES_FOLDER:-"${HOME}/Documents/GNL_Notes"}
#######################################################################################

if [[ "${NOTES_FOLDER}" == "" ]]; then
export NOTES_FOLDER=${HOME}/Documents/GNL_Notes
fi

NOTES_EDITOR=${NOTES_EDITOR:-nano}

# Create a new note in the note folder and
# open it using the default editor
NOTE() {
    mkdir -p ${NOTES_FOLDER}

    NOTE_FILE=$NOTES_FOLDER/$(date '+%Y-%m-%d').md
#    touch $NOTE_FILE

    if [[ ! -f "${NOTE_FILE}" ]]; then
cat <<EOT >> ${NOTE_FILE}
# Title

# To Do

- [x] Check
- [ ] unckeced

# Daily Log

EOT
        echo "CACHE_FOLDER variable is empty"
    fi 
    ${NOTES_EDITOR} ${NOTE_FILE}
}

TODO() {
    mkdir -p ${NOTES_FOLDER}

    NOTE_FILE=$NOTES_FOLDER/TODO.md

    if [[ ! -f "${NOTE_FILE}" ]]; then
cat <<EOT >> ${NOTE_FILE}
# To Do

- [x] Check
- [ ] unckeced


EOT
        echo "CACHE_FOLDER variable is empty"
    fi 
    ${NOTES_EDITOR} ${NOTE_FILE}
}

WTODO() {
    mkdir -p ${NOTES_FOLDER}

    NOTE_FILE=$NOTES_FOLDER/WTODO.md

    if [[ ! -f "${NOTE_FILE}" ]]; then
cat <<EOT >> ${NOTE_FILE}
# To Do

- [x] Check
- [ ] unckeced


EOT
        echo "CACHE_FOLDER variable is empty"
    fi 
    ${NOTES_EDITOR} ${NOTE_FILE}
}
