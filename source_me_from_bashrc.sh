#######################################################################################
FILE_LOCATION="${BASH_SOURCE[0]}"
while [ -h "$FILE_LOCATION" ]; do # resolve $FILE_LOCATION until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
  FILE_LOCATION="$(readlink "$FILE_LOCATION")"
  [[ $FILE_LOCATION != /* ]] && FILE_LOCATION="$DIR/$FILE_LOCATION" # if $FILE_LOCATION was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

SOURCE_DIR="$( cd -P "$( dirname "$FILE_LOCATION" )" >/dev/null 2>&1 && pwd )"
export BASH_HELPERS_SOURCE_DIR=$SOURCE_DIR
#######################################################################################

alias bashhelpers='xdg-open $BASH_HELPERS_SOURCE_DIR'


#######################################################################################


#######################################################################################
# C++ stuff
#######################################################################################
source ${SOURCE_DIR}/alias.sh

source ${SOURCE_DIR}/cpp.sh

source ${SOURCE_DIR}/browser.sh

source ${SOURCE_DIR}/prefix.sh

source ${SOURCE_DIR}/mount.sh

source ${SOURCE_DIR}/android_webcam.sh

source ${SOURCE_DIR}/keepass.sh

source ${SOURCE_DIR}/keepass2.sh

source ${SOURCE_DIR}/nix.sh

source ${SOURCE_DIR}/notes.sh
#######################################################################################




